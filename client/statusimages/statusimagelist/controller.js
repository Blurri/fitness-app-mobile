angular.module('fitnessapp')
  .controller('StatusimageListCtrl', function ($scope, $reactive, $rootScope, $state, $ionicActionSheet) {
    $reactive(this).attach($scope);

    this.helpers({
      statusimages: () => Statusimages.find({userId: Meteor.userId()}).fetch()
    })

    this.newStatusimage = () => {
      $state.go("sidemenu.newstatusimage");
    }


    this.openSheet = (id) => {
      $ionicActionSheet.show({
        buttons: [
          {text: 'Show'}
        ],
        cancelText: 'Cancel',
        cancel: () => {

        },
        buttonClicked: (index,el) => {
          if(index === 0){
            $state.go('sidemenu.statusimage',{id: id});
          }
          return true;
        }
      });
    }

  })
