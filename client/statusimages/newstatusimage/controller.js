
angular.module('fitnessapp')
  .controller('NewStatusimageCtrl', function ($scope, $reactive, $rootScope,$state) {
    $reactive(this).attach($scope);
    this.imageData = [];
    this.imageIndex = 0;
    this.comment = "";

    this.saveStatus = () => {
      var uploader = new Slingshot.Upload("mobileStatusImageUpload");
      for (var i = 0; i < this.imageData.length; i++) {
        var decoded = b64toBlob(this.imageData[i], 'image/jpeg');
        decoded.name = Random.id() + ".jpeg";
        Statusimages.insert({comment : this.comment, userId : Meteor.userId(), createdAt : new Date()}, (err, _id)=> {
          this.comment = "";
          this.imageData.map((file,index) => {
            uploader.send(decoded, function (error, downloadUrl) {
              if (error) {
                // Log service detailed response
                console.log(error);
              }
              else {
                Statusimages.update(_id, {$addToSet : {images : {url : downloadUrl}}});
              }
            });
          })
          this.imageData = [];
        });
      }
    }


    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }

    this.addImage = () => {
      if(this.imageIndex < 3){
        MeteorCamera.getPicture((err, data) => {
          if (err && err.error == 'cancel') {
            return;
          }
          this.imageData.push(data);
          this.imageIndex += 1;
          angular.element('#img'+this.imageIndex).prop('src', data);
          angular.element('#imgListItem'+this.imageIndex).show();
        });
      }
    }

  })
