angular.module('fitnessapp')
  .filter('exercisesleftfilter', () => {
    return function(item) {
      if(item !== undefined){
        return item.length + ' Left';
      }
    }
  })
