angular.module('fitnessapp')
  .controller('PlanListCtrl', function ($scope, $reactive, $rootScope, $ionicActionSheet, $state) {
    $reactive(this).attach($scope);
    this.helpers({
      plans: () => Plans.find({userId: Meteor.userId()},{sort : {createdAt : -1}}).fetch()
    });
    this.newPlan = () => {
      $state.go('sidemenu.newplan');
    }

    this.openSheet = (id) => {
      $ionicActionSheet.show({
       buttons: [
         { text: 'Edit' },
         { text: 'Training' }
       ],
       cancelText: 'Cancel',
       cancel: function() {
            // add cancel code..
          },
       buttonClicked: (index,el) => {
         if (index === 0){
           $state.go('sidemenu.editplan',{id : id})
         }else{
           let plan = Plans.findOne({_id: id});
           let training = Trainings.findOne({planId : plan._id, finished : false});
           if(training == undefined){
             Trainings.insert({planId: plan._id, finished : false, exercises : plan.exercises , finisedExercises : [], userId : Meteor.userId()},(err,_id) => {
               if(err){alert('error');return err;}
               $state.go('sidemenu.training' ,{id : _id});
             })
           }else{
             $state.go('sidemenu.training' ,{id : training._id});
           }
         }
         return true;
       }
      });
    }
  })
