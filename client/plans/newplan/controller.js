angular.module('fitnessapp')
  .controller('NewPlanCtrl', function ($scope, $reactive, $rootScope,$state) {
    $reactive(this).attach($scope);
    this.plantitle = "";
    this.createPlan = () => {
      if(this.plantitle !== ""){
        Plans.insert({title : this.plantitle, trainings : [], exercises : [], userId : Meteor.userId()},(err,id) => {
          if(err){return err;}
          $state.go('sidemenu.editplan', {id: id});
          this.plantitle = "";
        });
      }
    }
  })
