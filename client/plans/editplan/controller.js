angular.module('fitnessapp')
  .controller('EditPlanCtrl', function ($scope, $reactive, $rootScope,$state) {
    $reactive(this).attach($scope);
    this.helpers({
      plan: () => Plans.findOne({_id : $state.params.id}),
      exercises: () => Exercises.find({userId : Meteor.userId()}).fetch()
    })



    this.updatePlan = () => {

    }
    this.isInPlan = (id) => {
      return checkPlanExercises(id);
    }
    this.toggleExercise = (id) => {
      if(checkPlanExercises(id)) {
        removeExercise(id);
      }else{
        addExercise(id);
      }
    }
    var checkPlanExercises = (id) => {
      for (var i = 0; i < this.plan.exercises.length; i++) {
        if(this.plan.exercises[i] === id) {return true;}
      }
      return false;
    }
    var addExercise = (id) => {
      Plans.update(this.plan._id, {$addToSet : {exercises : id}}, (err,count) => {
        if(err){alert('UPDATE FAILED'); return err;}
      })
    }
    var removeExercise = (id) => {
      Plans.update(this.plan._id, {$pull : {exercises : id}}, (err,count) => {
        if(err){alert('UPDATE FAILED'); return err;}
      })
    }
  })
