angular.module('fitnessapp')
  .controller('ProfileCtrl', function($scope, $reactive, $rootScope) {
    let reactiveContext = $reactive(this).attach($scope);
    this.profileAvatar = {}
    this.showAvatar = false;
    this.profile = {}
    this.defaultDate = moment().toDate()

    this.changeselect = () => {
      Meteor.users.update(Meteor.userId(),{$set : {'profile.gender':this.profile.gender}})
    }

    this.dateDidChange = () => {
      Meteor.users.update(Meteor.userId(),{$set : {'profile.birthday':this.defaultDate}})
    }

    reactiveContext.helpers({
      user: ()  => {
        let user = Meteor.user();
        if (user != undefined){
          if(user.profile != undefined){
            this.profile = user.profile;
            if(user.profile.avatar != undefined){
              this.profileAvatar = user.profile.avatar;
              this.showAvatar = true;
            }
            if(user.profile.birthday){
               this.defaultDate = user.profile.birthday;
            }
          }
        }
        return user;
      }
    });


    this.uploadImage = (data) => {
      this.profileData = data;
      this.showProfile = true;
      var uploader = new Slingshot.Upload("mobileProfileImageUpload");
      var decoded = b64toBlob(data, 'image/jpeg');
      decoded.name = Random.id() + ".jpeg";
      uploader.send(decoded, function(error, downloadUrl) {
        if (error) {
          console.log(error);
        } else {
          Meteor.users.update(Meteor.userId(), {
            $addToset: {
              'profile.avatar': downloadUrl
            }
          });
        }
      });
    }



    this.addImage = () => {
      console.log('HOI');
      MeteorCamera.getPicture((err, data) => {
        if (err && err.error == 'cancel') {
          return;
        }
        this.uploadImage(data);
        angular.element('#avatar').prop('src', data);
      });
    }


    function b64toBlob(b64Data, contentType) {
      contentType = contentType || '';
      var sliceSize = 512;
      b64Data = b64Data.replace(/^[^,]+,/, '');
      b64Data = b64Data.replace(/\s/g, '');
      var byteCharacters = window.atob(b64Data);
      var byteArrays = [];
      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }
      var blob = new Blob(byteArrays, {
        type: contentType
      });
      return blob;
    }
  })
