angular.module('fitnessapp')
  .controller('TrainingCtrl', function ($scope, $reactive, $rootScope, $state) {
    let reactiveContext = $reactive(this).attach($scope);
    this.currentExercise = {};
    this.exerciseList = [];
    reactiveContext.helpers({
      training: ()  => {
        return Trainings.findOne({_id : $state.params.id,finished : false});
      },
      exercises: () => {
        let training = Trainings.findOne({_id : $state.params.id,finished : false});
        if(training !== undefined){
          let exercises = Exercises.find({_id : {$in : training.exercises }}).fetch();
          if (_.isEmpty(this.exercise)){
            this.exerciseList = exercises;
            this.currentExercise = exercises[0];
          }
          return exercises;
        }
      }
    });


    this.done = (exerciseId) => {
      const index = this.getIndexOfId(exerciseId);
      this.updateExercise();
      if(this.exerciseList.length === 1){
        Trainings.update(this.training._id, {$pull : {exercises : exerciseId},$push : {finisedExercises : exerciseId},$set : {finished : true}});
        $state.go('sidemenu.plans');
      }else{
        if (index === (this.exerciseList.length - 1)){
          this.exerciseList.splice(index, 1);
          this.currentExercise = this.exerciseList[0];
        }else {
          this.exerciseList.splice(index, 1);
          this.currentExercise = this.exerciseList[index];
        }

        Trainings.update(this.training._id, {$pull : {exercises : exerciseId},$push : {finisedExercises : exerciseId}});
      }

    }

    this.getIndexOfId = (exerciseId) => {
      for (var index = 0; index < this.exerciseList.length; index++) {
        var exercise = this.exerciseList[index];
        if(exerciseId === exercise._id){
          return index;
        }
      }
    }

    this.updateExercise = () => {
      Meteor.call('updateExercise', this.currentExercise);
    }

    this.another = (exerciseId) => {
      const index = this.getIndexOfId(exerciseId);
      if(index === (this.exerciseList.length - 1)){
        this.currentExercise = this.exerciseList[0];
      }else{
        this.currentExercise = this.exerciseList[index + 1];
      }
    }

  })
