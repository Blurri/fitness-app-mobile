angular.module('fitnessapp')
  .directive('sidemenu', function() {
    return {
      restrict: 'E',
      templateUrl: 'client/sidemenu/template.html',
      controllerAs: 'sidemenu',
      controller: function($scope, $reactive, $rootScope, $state) {
        $reactive(this).attach($scope);

        this.logout = () => {
          Meteor.logout();
          $state.go('login');
        }

      }
    }
  })
