angular.module('fitnessapp')
  .controller('EditMeasurementCtrl', function ($scope, $reactive, $rootScope, $state) {
    $reactive(this).attach($scope);

    this.helpers({
      measurement: () => Measurements.findOne({_id : $state.params.id})
    })

    this.updateMeasurement = () => {
      this.measurement.updatedAt = new Date();
      Measurements.update({_id : this.measurement._id},this.measurement,(err,_id)=>{
        if(err){return err};
      })
    }


  })
