angular.module('fitnessapp')
  .controller('NewMeasurementCtrl', function ($scope, $reactive, $rootScope, $state) {
    $reactive(this).attach($scope);

    this.measurement = {};

    this.newMeasurement = () => {
      this.measurement.createdAt = new Date();
      this.measurement.user = Meteor.userId();
      Measurements.insert(this.measurement,(err,_id)=>{
        if(err){return err};
        this.measurement = {};
        $state.go('sidemenu.measurements');
      })
    }

  })
