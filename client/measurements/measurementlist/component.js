angular.module('fitnessapp')
  .controller('MeasurementListCtrl', function ($scope, $reactive, $rootScope, $state, $ionicActionSheet) {
    $reactive(this).attach($scope);
    this.helpers({
      measurements: () => Measurements.find({user : Meteor.userId()}, {sort : {createdAt : -1}}).fetch()
    })


    this.newMeasurement = () => {
      $state.go("sidemenu.newmeasurement");
    }


    this.openSheet = (id) => {
      $ionicActionSheet.show({
        buttons: [
          {text: 'Edit'}
        ],
        cancelText: 'Cancel',
        cancel: () => {

        },
        buttonClicked: (index,el) => {
          if(index === 0){
            $state.go('sidemenu.editmeasurement',{id: id});
          }
          return true;
        }
      });
    }

  })
  .filter('fromNow', function() {
    return function(item) {
      return moment(item).fromNow()
    }
  })
