angular.module('fitnessapp')
  .controller('EditExercisesCtrl', function ($scope, $reactive, $rootScope, $state) {
    $reactive(this).attach($scope);

    this.helpers({
      exercise: () => Exercises.findOne({_id : $state.params.id})
    });


    this.updateExercise = () => {
      let exercise = Exercises.findOne({_id : $state.params.id});
      let uV = {title : this.exercise.title,type : this.exercise.type,weight : this.exercise.weight,duration : this.exercise.duration,numberOfSets : this.exercise.numberOfSets,repsInSet : this.exercise.repsInSet,description : this.exercise.description,distance : this.exercise.distance};
      exercise.weight ? null :  (this.exercise.weight ? uV.start_weight = {value : this.exercise.weight, createdAt : new Date()} :null);
      exercise.duration ? null :  (this.exercise.duration ? uV.start_duration = {value : this.exercise.duration, createdAt : new Date()} :null);
      exercise.distance ? null :  (this.exercise.distance ? uV.start_distance = {value : this.exercise.distance, createdAt : new Date()} :null);
      exercise.numberOfSets ? null :  (this.exercise.numberOfSets ? uV.start_numberOfSets = {value : this.exercise.numberOfSets, createdAt : new Date()} :null);
      exercise.repsInSet ? null :  (this.exercise.repsInSet ? uV.start_repsInSet = {value : this.exercise.repsInSet, createdAt : new Date()} :null);

      Exercises.update(this.exercise._id, {$set : uV}, (err, count) => {
        if(err) {alert('ERROR SAVING');return err};
        $state.go('sidemenu.exercises');
      });
    }

  })
