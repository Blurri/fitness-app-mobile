angular.module('fitnessapp')
  .controller('NewExerciseCtrl', function ($scope, $reactive, $rootScope, $state) {
    $reactive(this).attach($scope);

    this.exercise = {};

    this.createExercise = () => {
      this.exercise.weight ? this.exercise.start_weight = {value : this.exercise.weight, createdAt : new Date()} : null;
      this.exercise.duration ? this.exercise.start_duration = {value : this.exercise.duration, createdAt : new Date()} : null;
      this.exercise.numberOfSets ? this.exercise.start_numberOfSets = {value : this.exercise.numberOfSets, createdAt : new Date()} : null;
      this.exercise.repsInSet ? this.exercise.start_repsInSet = {value : this.exercise.repsInSet, createdAt : new Date()} : null;
      this.exercise.distance ? this.exercise.start_distance = {value : this.exercise.distance, createdAt : new Date()} : null;
      this.exercise.createdAt = new Date();
      this.exercise.userId = Meteor.userId();
      Exercises.insert(this.exercise , (err, exerciseId) => {
        if(err){
          alert('ERROR SAVING');
          return err;
        }
        this.exercise = {};
        $state.go('sidemenu.exercises');
      })
    }
  })
