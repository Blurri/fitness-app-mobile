angular.module('fitnessapp')
  .controller('ExercisesListCtrl', function ($scope, $reactive, $rootScope, $ionicActionSheet, $state) {
    $reactive(this).attach($scope)
    this.helpers({
      exercises: () => Exercises.find({userId : Meteor.userId()}, {sort : {createdAt : -1}}).fetch()
    });

    this.newExercise = () => {
      console.log('hello');
      $state.go('sidemenu.newexercise');
    }

    this.openSheet = (id) => {
      $ionicActionSheet.show({
        buttons: [
          {text: 'Edit'}
        ],
        cancelText: 'Cancel',
        cancel: () => {

        },
        buttonClicked: (index,el) => {
          if(index === 0){
            $state.go('sidemenu.editexercise', {id: id});
          }
          return true;
        }
      })
    }
  })
