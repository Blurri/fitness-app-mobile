angular.module('fitnessapp')
  .config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('sidemenu', {
        url: '/sidemenu',
        template: '<sidemenu></sidemenu>',
        abstract: true,
        resolve: ensureLogin
      })
      .state('sidemenu.profile', {
        url: '/profile',
        views : { 'menuContent' : { templateUrl : 'client/profile/template.html', controller : 'ProfileCtrl as ctrl'} }
      })
      // Start Plan routes
      .state('sidemenu.plans', {
        url: '/plans',
        views : { 'menuContent' : { templateUrl : 'client/plans/planlist/template.html', controller : 'PlanListCtrl as ctrl'} }
      })
      .state('sidemenu.editplan', {
        url : '/editplan/:id',
        views : { 'menuContent' : { templateUrl : 'client/plans/editplan/template.html', controller : 'EditPlanCtrl as ctrl'} }
      })
      .state('sidemenu.newplan', {
        url : '/newplan/:id',
        views : { 'menuContent' : { templateUrl : 'client/plans/newplan/template.html', controller: 'NewPlanCtrl as ctrl'} }
      })
      // End Plan routes
      // Start Exercise routes
      .state('sidemenu.exercises', {
        url: '/exercises',
        views : { 'menuContent' : { templateUrl : 'client/exercises/exerciselist/template.html', controller: 'ExercisesListCtrl as ctrl' } }
      })
      .state('sidemenu.newexercise', {
        url: '/newexercise',
        views : { 'menuContent' : { templateUrl : 'client/exercises/newexercise/template.html', controller: 'NewExerciseCtrl as ctrl' } }
      })
      .state('sidemenu.editexercise', {
        url: '/editexercise/:id',
        views : { 'menuContent' : { templateUrl : 'client/exercises/editexercise/template.html', controller: 'EditExercisesCtrl as ctrl' } }
      })
      // End Exercise routes
      // Start Measurements
      .state('sidemenu.measurements', {
        url: '/measurements',
        views : { 'menuContent' : { templateUrl : 'client/measurements/measurementlist/template.html', controller: 'MeasurementListCtrl as ctrl' } }
      })
      .state('sidemenu.editmeasurement', {
        url: '/editmeasurements/:id',
        views : { 'menuContent' : { templateUrl : 'client/measurements/editmeasurement/template.html', controller: 'EditMeasurementCtrl as ctrl' } }
      })
      .state('sidemenu.newmeasurement', {
        url: '/newmeasurement',
        views : { 'menuContent' : { templateUrl : 'client/measurements/newmeasurement/template.html', controller: 'NewMeasurementCtrl as ctrl' } }
      })
      // End Measurements

      // Start Statusimages
      .state('sidemenu.statusimages', {
        url: '/statusimages',
        views : { 'menuContent' : { templateUrl : 'client/statusimages/statusimagelist/template.html', controller: 'StatusimageListCtrl as ctrl' } }
      })
      .state('sidemenu.newstatusimage', {
        url: '/newstatusimage',
        views : { 'menuContent' : { templateUrl : 'client/statusimages/newstatusimage/template.html', controller: 'NewStatusimageCtrl as ctrl' } }
      })
      .state('sidemenu.statusimage', {
        url: '/statusimage/:id',
        views : { 'menuContent' : { templateUrl : 'client/statusimages/statusimage/template.html', controller: 'StatusimageCtrl as ctrl' } }
      })
      // End Statusimages

      .state('sidemenu.training', {
        url : '/training/:id',
        views : { 'menuContent' : { templateUrl : 'client/training/template.html', controller: 'TrainingCtrl as ctrl' } }
      })
      .state('login', {
        url : '/login',
        template : '<login></login>'
      })
    $urlRouterProvider.otherwise("/sidemenu/plans");
  })



let ensureLogin = {
	user: ($q) => {
		if (Meteor.userId() == null) {
            return $q.reject('AUTH_REQUIRED');
          } else {
            return $q.resolve();
          }
        }
}
